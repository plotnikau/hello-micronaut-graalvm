package hello.world.java;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;

import java.util.Date;

@Controller("/")
public class HelloController {

    @Get("/")
    @Produces(MediaType.TEXT_PLAIN)
    public String index() {
        String now = new Date().toString();
        return "Hello "+now;
    }
}
